import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

public class kadai24 {
    private JPanel root;
    private JButton tempuraButton;
    private JButton ramenButton;
    private JButton udonButton;
    private JButton gyozaButton;
    private JButton karaageButton;
    private JButton tonkatuButton;
    private JButton checkOutButton;
    private JTextPane orderedList;
    private JLabel totalLabel;

    int totalPrice = 0;

    public kadai24() {
        root.addComponentListener(new ComponentAdapter() {
        });
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura", 500);
            }
        });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen",900);
            }
        });
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon",550);
            }
        });
        gyozaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Gyoza",400);
            }
        });
        karaageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Karaage",450);
            }
        });
        tonkatuButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tonkatu",600);
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to checkout?",
                        "Checkout Confimation",
                        JOptionPane.YES_NO_OPTION
                );

                if (confirmation == 0) {
                    JOptionPane.showMessageDialog(null, "Thank you! The total price is "
                            + totalPrice + "yen!");
                }
                totalPrice=0;
                totalLabel.setText("Total "+totalPrice+" yen");

                String items = null;
                orderedList.setText(items);

            }
        });
        orderedList.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                super.componentResized(e);
            }
        });
        totalLabel.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                super.componentResized(e);
            }
        });

        ramenButton.setIcon(new ImageIcon(
                this.getClass().getResource("Ramen.jpg")
        ));
        tempuraButton.setIcon(new ImageIcon(
                this.getClass().getResource("Tempura.jpg")
        ));
        udonButton.setIcon(new ImageIcon(
                this.getClass().getResource("Udon.jpg")
        ));
        gyozaButton.setIcon(new ImageIcon(
                this.getClass().getResource("Gyouza.jpg")
        ));
        karaageButton.setIcon(new ImageIcon(
                this.getClass().getResource("Karaag.jpg")
        ));
        tonkatuButton.setIcon(new ImageIcon(
                this.getClass().getResource("Tonkatu.jpg")
        ));


    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("kadai2");
        frame.setContentPane(new kadai24().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }


    void order(String foodName, int foodPrice) {
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order " + foodName + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION
        );

        if (confirmation == 0) {
            int eatplace = JOptionPane.showConfirmDialog(
                    null,
                    "Would you like to eat " + foodName + " in the store?",
                    "eat place",
                    JOptionPane.YES_NO_OPTION
            );
            int TaxPrice=0;
            if (eatplace == 0) {
                TaxPrice=(int)(foodPrice*1.1);
                totalPrice += TaxPrice;

                String currentText = orderedList.getText();
                orderedList.setText(currentText + foodName + " " + TaxPrice + " yen\n");

                JOptionPane.showMessageDialog(null, "Order for " + foodName + " It will served as soon as possible.");

                totalLabel.setText("Total " + totalPrice + "yen");
            }
            if (eatplace == 1) {
                TaxPrice=(int)(foodPrice*1.08);
                totalPrice +=TaxPrice;

                String currentText = orderedList.getText();
                orderedList.setText(currentText + foodName + " " + TaxPrice + " yen\n");

                JOptionPane.showMessageDialog(null, "Order for " + foodName + " It will served as soon as possible.");

                totalLabel.setText("Total " + totalPrice + "yen");
            }
        }
    }
}
